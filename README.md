# README #

### What is this repository for? ###

* This repository contains Fortran programs that read the Australian Bureau of Meteorology (BoM) BARRA reanalysis files and write output data to the WRF intermediate data format. 
* For information on the WRF intermediate data format see Chapter 3 of the WRF User's Guide.

### How do I get set up? ###

* The headers of the individual Fortran files should contain all the information necessary for their use. 
* While these programs work correctly for the BARRA data for which they were initially written, it is quite possible that they may require modification by the user for some forms of BARRA data.  
* In particular, they currently work on data files that contain data for a single time step. Thus if the data files contain data for multiple time steps then the user will need to either split each of   
these files into single-time-step files or make easy modifications to the Fortran programs. 

### Who do I talk to? ###


* If you have questions regarding these programs contact their author christopherthomas@cmt.id.au

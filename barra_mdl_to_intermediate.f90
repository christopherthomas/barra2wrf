! Reads the BoM BARRA model-level data and outputs files in the WPS intermediate format. For
! information on the WPS intermediate format see Ch. 3 of the WRF Users Guide.
!
! At the moment it handles only the analysis data (ie one time per input file)
!
! NOTE: For BARRA pressure-level data use instead program barra_prs_to_intermediate which is
!       documented separately. 
!
! Useage: echo datadir datetime | barra_mdl_to_intermediate.exe
!
! where:
!
! datadir  is a directory containing the BARRA files or a directory of links to the BARRA files
! datetime is the datetime stamp in the form yyyymmddThhmmZ for example 20170409T0000Z
!          (note the T and Z are obligatory). This datetime string forms part of the file name
!          of the BARRA input file.
!
! When invoked like this the code will produce a single file in the WPS intermediate format with 
! the prefix 'BARRA_MDL' and containing data at the given time.
!
! To produce a dataset that is useable by metgrid.exe and real.exe, the user will also
! have to run barra_sfc_to_intermediate and barra_static_to_intermediate. These programs
! are structured in a similar way and are also documented in their headers. 
!
! Required BARRA filenames are:
!
! air_temp-yyyymmddThhmmZ
! spec_humd-yyyymmddThhmmZ
! pressure-yyyymmddThhmmZ
! height_theta
! wnd_ucmp-yyyymmddThhmmZ
! wnd_vcmp-yyyymmddThhmmZ
!
! These names are hard coded. They are related to, but are NOT, the standard names of files in the BARRA dataset. 
! The user will have to set up soft links in datadir using these names to link to the actual BARRA files 
!
! The code here writes all fields to the BARRA theta vertical levels. In the dataset for which this
! code was developed, all the above variables are on theta levels with the exception
! of the u and v winds, which required interpolating (linearly in height) to the theta levels.
! In addition, the spec_humd data are on theta levels but have an additional level at the surface.
! This level was dropped. Linear interpolation was used at the suggestion of Dr Chun-Hsu Su of BoM
! (private communication). If a different interpolation method is desired then the user can modify 
! the code appropriately.
!
! NOTE: It is PROBABLY true that all model-level BARRA datasets have this same structure,
!       but it MAY NOT BE! It is up to the user to check the BARRA netcdf files and modify this
!       code as required.
!
! Compile with something like:
!
! module purge
! module load intel-compiler/2020.0.166
! module load netcdf/4.7.1
! INCLUDEDIRS="-I/apps/netcdf/4.7.1/include/" 
! LIBDIRS="-L/apps/netcdf/4.7.1/lib/" 
! ifort -o barra_mdl_to_intermediate.exe  $INCLUDEDIRS $LIBDIRS -lnetcdff  -convert big_endian barra_mdl_to_intermediate.f90
!
! NOTE: The big_endian flag is required on little_endian systems.
!
! Author: Christopher Thomas 04/03/2019
! Please send questions, suggestions, bug reports etc to christopherthomas@cmt.id.au
!

module constants

  character(len=*), parameter  :: PREFIX = "BARRA_MDL"                            ! prefix of the intermediate-format output file 

  integer,parameter            :: OUTUNIT=10
  
  character(len=*),parameter   :: XDIMNAME='longitude'                            ! dimension and coordinate variable names 
  character(len=*),parameter   :: YDIMNAME='latitude'                             ! in the BARRA data files
  character(len=*),parameter   :: XVARN='longitude'                               
  character(len=*),parameter   :: YVARN='latitude'
  
  integer,parameter            :: IFV = 5                                         ! see WRF Users guide Ch 3 for an explanation
  integer,parameter            :: IPROJ = 0                                       ! of this block of constants
  real,parameter               :: EARTH_RADIUS = 6371.229
  logical,parameter            :: IS_WIND_GRID_REL = .false.
  character(len=32), parameter :: MAP_SOURCE = "BoM BARRA_R reanalysis          " 
  real, parameter              :: XFCST = 0.0
  character(len=8),parameter   :: STARTLOC = "SWCORNER"

  real,parameter               :: BARRA_FILL_VALUE_LIMIT = -1.E5                  ! data more negative than this will be treated
                                                                                  ! as missing values

end module constants

program barra_to_intemediate

  use netcdf
  use constants

  implicit none
  
  interface

     subroutine parse_input(input,datadir,datetime)
       character(len=288),intent(in)  :: input
       character(len=256),intent(out) :: datadir
       character(len=14),intent(out)  :: datetime
     end subroutine parse_input

     subroutine check(status)
       integer, intent(in) :: status
     end subroutine check
     
     subroutine get_xy_grid(fn,varn,nx,ny,startx,starty,dx,dy)
       character(len=*), intent(in) :: fn
       character(len=*), intent(in) :: varn
       real,intent(out)             :: startx,starty,dx,dy
       integer,intent(out)          :: nx,ny
     end subroutine get_xy_grid
     
     subroutine getdata1d(fn,varn,dimname,data1d)
       character(len=*), intent(in) :: fn
       character(len=*), intent(in) :: varn
       character(len=*), intent(in) :: dimname
       real,dimension(:),allocatable,intent(inout) :: data1d
     end subroutine getdata1d

     subroutine getdata2d(fn, varn, dimname1, dimname2, data2d) 
       character(len=*), intent(in)                  :: fn,varn,dimname1,dimname2
       real,dimension(:,:),allocatable,intent(inout) :: data2d
     end subroutine getdata2d

     subroutine getdata3d(fn,varn,dimname1,dimname2,dimname3,data3d) 
       character(len=*), intent(in)                    :: fn,varn,dimname1,dimname2,dimname3
       real,dimension(:,:,:),allocatable,intent(inout) :: data3d
     end subroutine getdata3d
     
     subroutine write_header(hdate, field, units, description, lvl, &
          nx, ny, startx, starty, dx, dy)
       character(len=24), intent(in)           :: hdate
       character(len=9),  intent(in)           :: field
       character(len=25), intent(in)           :: units 
       character(len=46), intent(in)           :: description 
       real,              intent(in)           :: lvl
       integer,           intent(in)           :: nx, ny
       real,              intent(in)           :: startx, starty, dx, dy
     end subroutine write_header

     subroutine write_slab(data,missing_value)
       real,dimension(:,:) :: data
       real,optional       :: missing_value
     end subroutine write_slab

     subroutine interp_lin(xi,vi,xo,vo)
       real, dimension(:), intent(in)    :: xi, vi, xo
       real, dimension(:), intent(inout) :: vo
     end subroutine interp_lin

  end interface


  character (len=288)                   :: input
  character (len=256)                   :: datadir
  character (len=14)                    :: datetime
  character (len=4)                     :: yr
  character (len=2)                     :: mth,day,hr

  integer                               :: nx, ny
  real                                  :: startx, starty, dx, dy
  real                                  :: lvl, wps_soil_lvl_top, wps_soil_lvl_bot
  real, dimension(:), allocatable       :: lvl_hgt, lvl_hgt_theta, sigma, sigma_theta, hgt, hgt_theta
  real, dimension(:,:), allocatable     :: data2d, depth_bnds, sfc_hgt
  real, dimension(:,:,:), allocatable   :: data3d, data_3d

  integer :: i,j,k,nlvl

  character(len=256)                    :: barra_fn
  character(len=256)                    :: out_fn
  character(len=16)                     :: barra_varn
  character(len=48)                     :: fn_prefix
  character(len=8)                      :: fn_suffix
  character(9)                          :: field
  character(46)                         :: description
  character(25)                         :: units
  character(24)                         :: hdate
 
  read '(a)', input
  call parse_input(input,datadir,datetime)

  write(*,*) trim(datadir), datetime

  yr = datetime(1:4)
  mth = datetime(5:6)
  day = datetime(7:8)
  hr = datetime(10:11)

  hdate = yr//':'//mth//':'//day//'_'//hr//':00:00     '

  !!!!! write 3D data 

  out_fn = PREFIX//':'//yr//'-'//mth//'-'//day//'_'//hr
  open(unit=OUTUNIT, file=out_fn, status='replace', form='unformatted')
  
  ! temperature specific humidity, pressure are all written on theta levels
  
  barra_varn = 'air_temp'
  barra_fn = trim(datadir)//'/'//'air_temp-'//datetime
  write(*,*) barra_fn
  field = 'TT'
  units       = "K"
  description = "Temperature"
  call get_xy_grid(barra_fn,barra_varn,nx,ny,startx,starty,dx,dy)
  call getdata3d(barra_fn,barra_varn,'model_level_number',YDIMNAME,XDIMNAME,data3d)
  nlvl = size(data3d,3)
  do k = 1, nlvl
     call write_header(hdate, field, units, description, real(nlvl-k+1), &
          nx, ny, startx, starty, dx, dy)
     call write_slab(data3d(:,:,k))
  end do
  deallocate(data3d)
  
  barra_varn = 'spec_hum'
  barra_fn = trim(datadir)//'/'//'spec_hum-'//datetime
  write(*,*) barra_fn
  field = 'SPECHUMD'
  units       = "kg kg-1"
  description = "Specific humidity"
  call get_xy_grid(barra_fn,barra_varn,nx,ny,startx,starty,dx,dy)
  call getdata3d(barra_fn,barra_varn,'model_level_number',YDIMNAME,XDIMNAME,data3d)
  !note: specific humidity vert level starts at 0.0 in barra file,
  !      so we omit the first level
  nlvl = size(data3d,3)
  do k = 2, nlvl  
     call write_header(hdate, field, units, description, real(nlvl-k+1), &
          nx, ny, startx, starty, dx, dy)
     call write_slab(data3d(:,:,k))
  end do
  deallocate(data3d)

  barra_varn  = 'pressure'
  barra_fn    = trim(datadir)//'/'//'pressure-'//datetime
  field       = 'PRESSURE'
  units       = "Pa"
  description = "Pressure"
  call get_xy_grid(barra_fn,barra_varn,nx,ny,startx,starty,dx,dy)
  call getdata3d(barra_fn,barra_varn,'model_level_number',YDIMNAME,XDIMNAME,data3d)
  nlvl = size(data3d,3)
  do k = 1, nlvl
     call write_header(hdate, field, units, description, real(nlvl-k+1), &
          nx, ny, startx, starty, dx, dy)
     call write_slab(data3d(:,:,k))
  end do
  deallocate(data3d)
 
  barra_varn  = 'height_theta'
  barra_fn    = trim(datadir)//'/'//'height_theta'
  write(*,*) barra_fn
  field       = 'GHT'
  units       = "m"
  description = "Geopotential height"
  call get_xy_grid(barra_fn,barra_varn,nx,ny,startx,starty,dx,dy)
  call getdata3d(barra_fn,barra_varn,'model_level_number',YDIMNAME,XDIMNAME,data3d)
  nlvl = size(data3d,3)
  do k = 1, nlvl
     call write_header(hdate, field, units, description, real(nlvl-k+1), &
          nx, ny, startx, starty, dx, dy)
     call write_slab(data3d(:,:,k))
  end do
  deallocate(data3d)

  ! winds are not on theta levels in the BARRA files, so we interpolate linearly in height
  ! to the theta levels. Note also that they are staggered horizontally as well
  ! as vertically. Consequently we have to compute the height of the theta levels
  ! above the u and v horizontal grid.

  ! get the parameters for the theta level heights
  barra_fn = trim(datadir)//'/'//'height_theta'
  call getdata1d(barra_fn,'level_height','model_level_number',lvl_hgt_theta)
  call getdata1d(barra_fn,'sigma','model_level_number',sigma_theta)

  barra_varn = 'wnd_ucmp'
  barra_fn   = trim(datadir)//'/'//'wnd_ucmp-'//datetime
  field = 'UU'
  units       = "m s-1"
  description = "U"
  call get_xy_grid(barra_fn,barra_varn,nx,ny,startx,starty,dx,dy)
  ! the parameters for the wind levels 
  call getdata1d(barra_fn,'level_height','model_level_number',lvl_hgt)
  call getdata1d(barra_fn,'sigma','model_level_number',sigma)
  call getdata2d(barra_fn,'surface_altitude',YDIMNAME,XDIMNAME,sfc_hgt)
  ! the input data on wind levels
  call getdata3d(barra_fn,barra_varn,'model_level_number',YDIMNAME,XDIMNAME,data3d)
  ! array to hold interpolated data on theta levels
  ! (same xy grid as wind data, vertical grid is the theta levels)
  allocate(data_3d(size(data3d,1),size(data3d,2),size(lvl_hgt_theta)))
  allocate(hgt(size(lvl_hgt)))
  allocate(hgt_theta(size(lvl_hgt_theta)))
  !loop over the pixels
  do i = 1, size(data_3d,1)
     do j = 1, size(data_3d,2)
        !height of the wind levels above i,j
        hgt = lvl_hgt + sigma*sfc_hgt(i,j)
        !height of the the theta levels above i,j
        hgt_theta = lvl_hgt_theta + sigma_theta*sfc_hgt(i,j) 
        !interpolate to theta levels
        call interp_lin(lvl_hgt,data3d(i,j,:),lvl_hgt_theta,data_3d(i,j,:))
     end do
  end do
  !write out the slabs
  nlvl = size(data_3d,3)
  do k = 1, nlvl
     call write_header(hdate, field, units, description, real(nlvl-k+1), &
          nx, ny, startx, starty, dx, dy)
     call write_slab(data_3d(:,:,k))
  end do
  deallocate(hgt_theta)
  deallocate(lvl_hgt)
  deallocate(sigma)
  deallocate(hgt)
  deallocate(sfc_hgt)
  deallocate(data3d)
  deallocate(data_3d)

  barra_varn = 'wnd_vcmp'
  barra_fn   = trim(datadir)//'/'//'wnd_vcmp-'//datetime
  field = 'VV'
  units       = "m s-1"
  description = "V"
  call get_xy_grid(barra_fn,barra_varn,nx,ny,startx,starty,dx,dy)
  ! the parameters for the wind levels 
  call getdata1d(barra_fn,'level_height','model_level_number',lvl_hgt)
  call getdata1d(barra_fn,'sigma','model_level_number',sigma)
  call getdata2d(barra_fn,'surface_altitude',YDIMNAME,XDIMNAME,sfc_hgt)
  ! the input data on wind levels
  call getdata3d(barra_fn,barra_varn,'model_level_number',YDIMNAME,XDIMNAME,data3d)
  ! array to hold interpolated data on theta levels
  ! (same xy grid as wind data, vertical grid is the theta levels)
  allocate(data_3d(size(data3d,1),size(data3d,2),size(lvl_hgt_theta)))
  allocate(hgt(size(lvl_hgt)))
  allocate(hgt_theta(size(lvl_hgt_theta)))
  !loop over the pixels
  do i = 1, size(data_3d,1)
     do j = 1, size(data_3d,2)
        !height of the wind levels above i,j
        hgt = lvl_hgt + sigma*sfc_hgt(i,j)
        !height of the the theta levels above i,j
        hgt_theta = lvl_hgt_theta + sigma_theta*sfc_hgt(i,j) 
        !interpolate to theta levels
        call interp_lin(lvl_hgt,data3d(i,j,:),lvl_hgt_theta,data_3d(i,j,:))
     end do
  end do
  !write out the slabs
  nlvl = size(data_3d,3)
  do k = 1, nlvl
     call write_header(hdate, field, units, description, real(nlvl-k+1), &
          nx, ny, startx, starty, dx, dy)
     call write_slab(data_3d(:,:,k))
  end do
  deallocate(hgt_theta)
  deallocate(lvl_hgt)
  deallocate(sigma)
  deallocate(hgt)
  deallocate(sfc_hgt)
  deallocate(data3d)
  deallocate(data_3d)

  deallocate(lvl_hgt_theta)
  deallocate(sigma_theta)

  close(OUTUNIT)
  
end program barra_to_intemediate

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine parse_input(input,datadir,datetime)

  implicit none

  character(len=288),intent(in)  :: input
  character(len=256),intent(out) :: datadir
  character(len=14),intent(out)  :: datetime

  integer :: n

  n = index(input,' ')
  datadir = input(1:n-1)
  datetime = trim(adjustl(input(n:288)))

  
end subroutine parse_input

subroutine check(status)

  use netcdf
  use constants
  
  implicit none

  integer, intent(in) :: status

  if(status /= nf90_noerr) then 
     print *, trim(nf90_strerror(status))
     stop "Stopped"
  end if

end subroutine check

subroutine get_xy_grid(fn,varn,nx,ny,startx,starty,dx,dy)

  use netcdf
  use constants
  
  implicit none

  character(len=*), intent(in) :: fn
  character(len=*), intent(in) :: varn
  real,intent(out) :: startx,starty,dx,dy
  integer,intent(out) :: nx,ny

  integer :: ncid,dimid,varid
  real,dimension(:),allocatable :: data

  call check(nf90_open(trim(fn),nf90_nowrite,ncid))
  call check(nf90_inq_dimid(ncid,XDIMNAME,dimid))
  call check(nf90_inquire_dimension(ncid,dimid,len=nx))
  allocate(data(nx))
  call check(nf90_inq_varid(ncid,XVARN,varid))
  call check(nf90_get_var(ncid, varid, data))
  startx = data(1)
  dx = data(2) - data(1)
  deallocate(data)
  
  call check(nf90_inq_dimid(ncid,YDIMNAME,dimid))
  call check(nf90_inquire_dimension(ncid,dimid,len=ny))
  allocate(data(ny))
  call check(nf90_inq_varid(ncid,YVARN,varid))
  call check(nf90_get_var(ncid, varid, data))
  starty = data(1)
  dy = data(2) - data(1)
  deallocate(data)

  call check(nf90_close(ncid))

end subroutine get_xy_grid

subroutine getdata1d(fn,varn,dimname,data1d)

  use netcdf
  use constants
  
  implicit none

  character(len=*), intent(in) :: fn
  character(len=*), intent(in) :: varn
  character(len=*), intent(in) :: dimname
  real,dimension(:),allocatable,intent(inout) :: data1d

  integer :: ncid,dimid,varid,n

  call check(nf90_open(trim(fn),nf90_nowrite,ncid))
  
  ! get the dimension and allocate array
  call check(nf90_inq_dimid(ncid,dimname,dimid))
  call check(nf90_inquire_dimension(ncid,dimid,len=n))
  allocate(data1d(n))

  ! get the data
  call check(nf90_inq_varid(ncid,varn,varid))
  call check(nf90_get_var(ncid, varid, data1d))
  call check(nf90_close(ncid))

end subroutine getdata1d

subroutine getdata2d(fn, varn, dimname1, dimname2, data2d) 

  use netcdf
  use constants

  implicit none

  character(len=*), intent(in) :: fn,varn,dimname1,dimname2
  real,dimension(:,:),allocatable,intent(inout) :: data2d

  integer :: ncid,dimid,varid,n1,n2

  call check(nf90_open(trim(fn),nf90_nowrite,ncid))

  ! get the dimensions and allocate array
  call check(nf90_inq_dimid(ncid,dimname1,dimid))
  call check(nf90_inquire_dimension(ncid,dimid,len=n1))
  call check(nf90_inq_dimid(ncid,dimname2,dimid))
  call check(nf90_inquire_dimension(ncid,dimid,len=n2))
  allocate(data2d(n2,n1))

  ! get the data
  call check(nf90_inq_varid(ncid,varn,varid))
  call check(nf90_get_var(ncid, varid, data2d))
  call check(nf90_close(ncid))

end subroutine getdata2d

subroutine getdata3d(fn,varn,dimname1,dimname2,dimname3,data3d) 

  use netcdf
  use constants

  implicit none

  character(len=*), intent(in) :: fn,varn,dimname1,dimname2,dimname3
  real,dimension(:,:,:),allocatable,intent(inout) :: data3d

  integer :: ncid,dimid,varid,n1,n2,n3

  call check(nf90_open(trim(fn),nf90_nowrite,ncid))

  ! get the dimensions and allocate array
  call check(nf90_inq_dimid(ncid,dimname1,dimid))
  call check(nf90_inquire_dimension(ncid,dimid,len=n1))
  call check(nf90_inq_dimid(ncid,dimname2,dimid))
  call check(nf90_inquire_dimension(ncid,dimid,len=n2))
  call check(nf90_inq_dimid(ncid,dimname3,dimid))
  call check(nf90_inquire_dimension(ncid,dimid,len=n3))
  allocate(data3d(n3,n2,n1))

  ! get the data
  call check(nf90_inq_varid(ncid,varn,varid))
  call check(nf90_get_var(ncid, varid, data3d))
  call check(nf90_close(ncid))

end subroutine getdata3d

subroutine write_header(hdate, field, units, description, xlvl, &
     nx, ny, startx, starty, dx, dy)

  use constants

  character(len=24), intent(in)           :: hdate
  character(len=9),  intent(in)           :: field
  character(len=25), intent(in)           :: units 
  character(len=46), intent(in)           :: description 
  real,              intent(in)           :: xlvl
  integer,           intent(in)           :: nx, ny
  real,              intent(in)           :: startx, starty, dx, dy

  write(*,*) 'Writing data for ', field, ' at level ', xlvl

  write(outunit) IFV
  write(outunit) hdate, XFCST, MAP_SOURCE, field, units, description, xlvl , nx, ny, IPROJ
  write(outunit) STARTLOC, starty, startx, dy, dx, EARTH_RADIUS
  write(outunit) IS_WIND_GRD_REL

end subroutine write_header

subroutine write_slab(data,missing_value)

  use constants
  
  implicit none

  real,dimension(:,:) :: data
  real, optional    :: missing_value          

  integer :: i,j

  if (present(missing_value)) then
     do i = 1, size(data,1)
        do j = 1, size(data,2)
           if (data(i,j) <  BARRA_FILL_VALUE_LIMIT) then
              data(i,j) = missing_value
           end if
        end do
     end do
  end if

  write(OUTUNIT) data

end subroutine write_slab
  
subroutine interp_lin(xi,vi,xo,vo)
  ! depends on xi being monotonically increasing
  
  implicit none
  
  real, dimension(:), intent(in)    :: xi, vi, xo
  real, dimension(:), intent(inout) :: vo

  integer                           :: ki, ko, ni, ki_lo, ki_hi

  ni = size(xi)
  
  do ko = 1, size(xo)
     ! find the bracketing indices
     do ki = 2, ni
        ki_hi = ki
        if (xi(ki) > xo(ko)) then
           exit
        end if
     end do
     ki_lo = ki_hi - 1

     vo(ko) = vi(ki_lo) + (xo(ko)-xi(ki_lo))*(vi(ki_hi) - vi(ki_lo))/(xi(ki_hi) - xi(ki_lo))
     
  end do
  
end subroutine interp_lin




! Reads the BoM BARRA pressure-level data and outputs files in the WPS intermediate format. For
! information on the WPS intermediate format see Ch. 3 of the WRF Users Guide.
!
! At the moment it handles only the analysis data (ie one time per input file)
!
! NOTE: For BARRA model-level data use instead program barra_mdl_to_intermediate which is
!       documented separately. 
!
! Useage: echo datadir datetime | barra_prs_to_intermediate.exe
!
! where:
!
! datadir  is a directory containing the BARRA files or a directory of links to the BARRA files
! datetime is the datetime stamp in the form yyyymmddThhmmZ for example 20170409T0000Z
!          (note the T and Z are obligatory). This datetime string forms part of the file name
!          of the BARRA input file.
!
! When invoked like this the code will produce a single file in the WPS intermediate format with 
! the prefix 'BARRA_PRS' and containing data at the given time.
!
! To produce a dataset that is useable by metgrid.exe and real.exe, the user will also
! have to run barra_sfc_to_intermediate and barra_static_to_intermediate. These programs
! are structured in a similar way and are also documented in their headers. 
!
! Required BARRA filenames are:
!
! air_temp-yyyymmddThhmmZ
! wnd_ucmp-yyyymmddThhmmZ
! wnd_vcmp-yyyymmddThhmmZ
! relhum-yyyymmddThhmmZ
! geop_ht-yyyymmddThhmmZ
!
! These names are hard coded. They are related to, but are NOT, the standard names of files in the BARRA dataset. 
! The user will have to set up soft links in datadir using these names to link to the actual BARRA files 
!
! NOTE: It is PROBABLY true that all pressure-level BARRA datasets have the structure assumed by this program,
!       but it MAY NOT BE! It is up to the user to check the BARRA netcdf files and modify this
!       code as required.
!
! Compile with something like:
!
! module purge
! module load intel-compiler/2020.0.166
! module load netcdf/4.7.1
! INCLUDEDIRS="-I/apps/netcdf/4.7.1/include/" 
! LIBDIRS="-L/apps/netcdf/4.7.1/lib/" 
! ifort -o barra_prs_to_intermediate.exe  $INCLUDEDIRS $LIBDIRS -lnetcdff  -convert big_endian barra_prs_to_intermediate.f90
!
! NOTE: The big_endian flag is required on little_endian systems.
!
! Author: Christopher Thomas 04/03/2019
! Please send questions, suggestions, bug reports etc to christopherthomas@cmt.id.au
!

module constants

  character(len=*), parameter  :: PREFIX= "BARRA_PRS"                              ! prefix of the intermediate-format output file 
  integer,parameter            :: OUTUNIT=10
  
  character(len=*),parameter   :: XDIMNAME='longitude'                             ! dimension and coordinate variable name
  character(len=*),parameter   :: YDIMNAME='latitude'                              ! in the BARRA data files
  character(len=*),parameter   :: XVARN='longitude'
  character(len=*),parameter   :: YVARN='latitude'
  
  integer,parameter            :: IFV = 5                                          ! see WRF Users guide Ch 3 for an explanation
  integer,parameter            :: IPROJ = 0                                        ! of this block of constants
  real,parameter               :: EARTH_RADIUS = 6371.229
  logical,parameter            :: IS_WIND_GRID_REL = .false.
  character(len=32), parameter :: MAP_SOURCE = "BoM BARRA_R reanalysis          "
  real, parameter              :: XFCST = 0.0
  character(len=8),parameter   :: STARTLOC = "SWCORNER"

  real,parameter               :: BARRA_FILL_VALUE_LIMIT = -1.E5                   ! data more negative than this will be treated
                                                                                   ! as missing values

end module constants

program barra_to_intemediate

  use netcdf
  use constants

  implicit none
  
  interface

     subroutine parse_input(input,datadir,datetime)
       character(len=288),intent(in)  :: input
       character(len=256),intent(out) :: datadir
       character(len=14),intent(out)  :: datetime
     end subroutine parse_input

     subroutine check(status)
       integer, intent(in) :: status
     end subroutine check
     
     subroutine get_xy_grid(fn,varn,nx,ny,startx,starty,dx,dy)
       character(len=*), intent(in) :: fn
       character(len=*), intent(in) :: varn
       real,intent(out) :: startx,starty,dx,dy
       integer,intent(out) :: nx,ny
     end subroutine get_xy_grid
     
     subroutine getdata1d(fn,varn,dimname,data1d)
       character(len=*), intent(in) :: fn
       character(len=*), intent(in) :: varn
       character(len=*), intent(in) :: dimname
       real,dimension(:),allocatable,intent(inout) :: data1d
     end subroutine getdata1d

     subroutine getdata3d(fn,varn,dimname1,dimname2,dimname3,data3d) 
       character(len=*), intent(in) :: fn,varn,dimname1,dimname2,dimname3
       real,dimension(:,:,:),allocatable,intent(inout) :: data3d
     end subroutine getdata3d
     
     subroutine write_header(hdate, field, units, description, lvl, &
          nx, ny, startx, starty, dx, dy)
       character(len=24), intent(in)           :: hdate
       character(len=9),  intent(in)           :: field
       character(len=25), intent(in)           :: units 
       character(len=46), intent(in)           :: description 
       real,              intent(in)           :: lvl
       integer,           intent(in)           :: nx, ny
       real,              intent(in)           :: startx, starty, dx, dy
     end subroutine write_header

     subroutine write_slab(data,missing_value)
       real,dimension(:,:) :: data
       real,optional       :: missing_value
     end subroutine write_slab

  end interface


  character (len=288) :: input
  character (len=256) :: datadir
  character (len=14)  :: datetime
  character (len=4)   :: yr
  character (len=2)   :: mth,day,hr

  integer :: nx,ny
  real    :: startx,starty,dx,dy
  real    :: lvl,wps_soil_lvl_top,wps_soil_lvl_bot
  real, dimension(:), allocatable :: lvls
  real, dimension(:,:), allocatable     :: data2d, depth_bnds
  real, dimension(:,:,:), allocatable :: data3d

  integer :: k

  character(len=256) :: barra_fn
  character(len=256) :: out_fn
  character(len=16) :: barra_varn
  character(len=4)   :: cat
  character(9)      :: field
  character(46)     :: description
  character(25)     :: units
  character(24)     :: hdate
 
  read '(a)', input
  call parse_input(input,datadir,datetime)

  write(*,*) trim(datadir), datetime

  yr = datetime(1:4)
  mth = datetime(5:6)
  day = datetime(7:8)
  hr = datetime(10:11)

  hdate = yr//':'//mth//':'//day//'_'//hr//':00:00     '

  !!!!! write 3D data 

  out_fn = PREFIX//':'//yr//'-'//mth//'-'//day//'_'//hr
  open(unit=OUTUNIT, file=out_fn, status='replace', form='unformatted')
  
  
  barra_varn = 'air_temp'
  barra_fn = trim(datadir)//'/'//'air_temp-'//datetime
  field = 'TT'
  units       = "K"
  description = "Temperature"
  write(*,*) barra_fn
  call get_xy_grid(barra_fn,barra_varn,nx,ny,startx,starty,dx,dy)
  call getdata1d(barra_fn,'pressure','pressure',lvls)
  call getdata3d(barra_fn,barra_varn,'pressure',YDIMNAME,XDIMNAME,data3d)
  lvls = 100.*lvls  !convert hPa to Pa
  do k = 1, size(data3d,3)
     call write_header(hdate, field, units, description, lvls(k), &
          nx, ny, startx, starty, dx, dy)
     call write_slab(data3d(:,:,k))
  end do
  deallocate(lvls)
  deallocate(data3d)
  
  barra_varn = 'wnd_ucmp'
  barra_fn = trim(datadir)//'/'//'wnd_ucmp-'//datetime
  field = 'UU'
  units       = "m s-1"
  description = "U"
  call get_xy_grid(barra_fn,barra_varn,nx,ny,startx,starty,dx,dy)
  call getdata1d(barra_fn,'pressure','pressure',lvls)
  call getdata3d(barra_fn,barra_varn,'pressure',YDIMNAME,XDIMNAME,data3d)
  lvls = 100.*lvls  !convert hPa to Pa
  do k = 1, size(data3d,3)
     call write_header(hdate, field, units, description, lvls(k), &
          nx, ny, startx, starty, dx, dy)
     call write_slab(data3d(:,:,k))
  end do
  deallocate(lvls)
  deallocate(data3d)
  
  barra_varn = 'wnd_vcmp'
  barra_fn = trim(datadir)//'/'//'wnd_vcmp-'//datetime
  field = 'VV'
  units       = "m s-1"
  description = "V"
  call get_xy_grid(barra_fn,barra_varn,nx,ny,startx,starty,dx,dy)
  call getdata1d(barra_fn,'pressure','pressure',lvls)
  call getdata3d(barra_fn,barra_varn,'pressure',YDIMNAME,XDIMNAME,data3d)
  lvls = 100.*lvls  !convert hPa to Pa
  do k = 1, size(data3d,3)
     call write_header(hdate, field, units, description, lvls(k), &
          nx, ny, startx, starty, dx, dy)
     call write_slab(data3d(:,:,k))
  end do
  deallocate(lvls)
  deallocate(data3d)

  
  barra_varn = 'relhum'
  barra_fn = trim(datadir)//'/'//'relhum-'//datetime
  field = 'RH'
  units       = "%"
  description = "Relative humidity"
  call get_xy_grid(barra_fn,barra_varn,nx,ny,startx,starty,dx,dy)
  call getdata1d(barra_fn,'pressure','pressure',lvls)
  call getdata3d(barra_fn,barra_varn,'pressure',YDIMNAME,XDIMNAME,data3d)
  lvls = 100.*lvls  !convert hPa to Pa
  do k = 1, size(data3d,3)
     call write_header(hdate, field, units, description, lvls(k), &
          nx, ny, startx, starty, dx, dy)
     call write_slab(data3d(:,:,k))
  end do
  deallocate(lvls)
  deallocate(data3d)
 
 
  barra_varn = 'geop_ht'
  barra_fn = trim(datadir)//'/'//'geop_ht-'//datetime
  field = 'GHT'
  units       = "m"
  description = "Geopotential height"
  call get_xy_grid(barra_fn,barra_varn,nx,ny,startx,starty,dx,dy)
  call getdata1d(barra_fn,'pressure','pressure',lvls)
  call getdata3d(barra_fn,barra_varn,'pressure',YDIMNAME,XDIMNAME,data3d)
  lvls = 100.*lvls
  do k = 1, size(data3d,3)
     call write_header(hdate, field, units, description, lvls(k), &
          nx, ny, startx, starty, dx, dy)
     call write_slab(data3d(:,:,k))
  end do
  deallocate(lvls)
  deallocate(data3d)

  close(OUTUNIT)
  
  !!!!! end write 3D data


end program barra_to_intemediate

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine parse_input(input,datadir,datetime)

  implicit none

  character(len=288),intent(in)  :: input
  character(len=256),intent(out) :: datadir
  character(len=14),intent(out)  :: datetime

  integer :: n

  n = index(input,' ')
  datadir = input(1:n-1)
  datetime = trim(adjustl(input(n:288)))

  
end subroutine parse_input

subroutine check(status)

  use netcdf
  use constants
  
  implicit none

  integer, intent(in) :: status

  if(status /= nf90_noerr) then 
     print *, trim(nf90_strerror(status))
     stop "Stopped"
  end if

end subroutine check

subroutine get_xy_grid(fn,varn,nx,ny,startx,starty,dx,dy)

  use netcdf
  use constants
  
  implicit none

  character(len=*), intent(in) :: fn
  character(len=*), intent(in) :: varn
  real,intent(out) :: startx,starty,dx,dy
  integer,intent(out) :: nx,ny

  integer :: ncid,dimid,varid
  real,dimension(:),allocatable :: data

  call check(nf90_open(trim(fn),nf90_nowrite,ncid))
  call check(nf90_inq_dimid(ncid,XDIMNAME,dimid))
  call check(nf90_inquire_dimension(ncid,dimid,len=nx))
  allocate(data(nx))
  call check(nf90_inq_varid(ncid,XVARN,varid))
  call check(nf90_get_var(ncid, varid, data))
  startx = data(1)
  dx = data(2) - data(1)
  deallocate(data)
  
  call check(nf90_inq_dimid(ncid,YDIMNAME,dimid))
  call check(nf90_inquire_dimension(ncid,dimid,len=ny))
  allocate(data(ny))
  call check(nf90_inq_varid(ncid,YVARN,varid))
  call check(nf90_get_var(ncid, varid, data))
  starty = data(1)
  dy = data(2) - data(1)
  deallocate(data)

  call check(nf90_close(ncid))

end subroutine get_xy_grid

subroutine getdata1d(fn,varn,dimname,data1d)

  use netcdf
  use constants
  
  implicit none

  character(len=*), intent(in) :: fn
  character(len=*), intent(in) :: varn
  character(len=*), intent(in) :: dimname
  real,dimension(:),allocatable,intent(inout) :: data1d

  integer :: ncid,dimid,varid,n

  call check(nf90_open(trim(fn),nf90_nowrite,ncid))
  
  ! get the dimension and allocate array
  call check(nf90_inq_dimid(ncid,dimname,dimid))
  call check(nf90_inquire_dimension(ncid,dimid,len=n))
  allocate(data1d(n))

  ! get the data
  call check(nf90_inq_varid(ncid,varn,varid))
  call check(nf90_get_var(ncid, varid, data1d))
  call check(nf90_close(ncid))

end subroutine getdata1d

subroutine getdata3d(fn,varn,dimname1,dimname2,dimname3,data3d) 

  use netcdf
  use constants

  implicit none

  character(len=*), intent(in) :: fn,varn,dimname1,dimname2,dimname3
  real,dimension(:,:,:),allocatable,intent(inout) :: data3d

  integer :: ncid,dimid,varid,n1,n2,n3

  call check(nf90_open(trim(fn),nf90_nowrite,ncid))

  ! get the dimensions and allocate array
  call check(nf90_inq_dimid(ncid,dimname1,dimid))
  call check(nf90_inquire_dimension(ncid,dimid,len=n1))
  call check(nf90_inq_dimid(ncid,dimname2,dimid))
  call check(nf90_inquire_dimension(ncid,dimid,len=n2))
  call check(nf90_inq_dimid(ncid,dimname3,dimid))
  call check(nf90_inquire_dimension(ncid,dimid,len=n3))
  allocate(data3d(n3,n2,n1))

  ! get the data
  call check(nf90_inq_varid(ncid,varn,varid))
  call check(nf90_get_var(ncid, varid, data3d))
  call check(nf90_close(ncid))

end subroutine getdata3d

subroutine write_header(hdate, field, units, description, xlvl, &
     nx, ny, startx, starty, dx, dy)

  use constants

  character(len=24), intent(in)           :: hdate
  character(len=9),  intent(in)           :: field
  character(len=25), intent(in)           :: units 
  character(len=46), intent(in)           :: description 
  real,              intent(in)           :: xlvl
  integer,           intent(in)           :: nx, ny
  real,              intent(in)           :: startx, starty, dx, dy

  write(*,*) 'Writing data for ', field, ' at level ', xlvl

  write(outunit) IFV
  write(outunit) hdate, XFCST, MAP_SOURCE, field, units, description, xlvl , nx, ny, IPROJ
  write(outunit) STARTLOC, starty, startx, dy, dx, EARTH_RADIUS
  write(outunit) IS_WIND_GRD_REL

end subroutine write_header

subroutine write_slab(data,missing_value)

  use constants
  
  implicit none

  real,dimension(:,:) :: data
  real, optional    :: missing_value          

  integer :: i,j

  if (present(missing_value)) then
     do i = 1, size(data,1)
        do j = 1, size(data,2)
           if (data(i,j) <  BARRA_FILL_VALUE_LIMIT) then
              data(i,j) = missing_value
           end if
        end do
     end do
  end if

  write(OUTUNIT) data

end subroutine write_slab
  





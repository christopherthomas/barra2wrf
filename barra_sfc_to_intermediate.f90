! Reads the BoM BARRA surface-level data and outputs files in the WPS intermediate format. For
! information on the WPS intermediate format see Ch. 3 of the WRF Users Guide.
!
! At the moment it handles only the analysis data (ie one time per input file)
!
! Useage: echo datadir datetime | barra_sfc_to_intermediate.exe
!
! where
!
! datadir  is a directory containing the BARRA files or a directory of links to the BARRA files    
! datetime is the datetime stamp in the form yyyymmddThhmmZ for example 20170409T0000Z
!          (note the T and Z are obligatory). This datetime string forms part of the filename
!          of the BARRA input file
!
! This When invoked like this the code will produce a single file in the WPS intermediate format with 
! the prefix 'BARRA_SFC' and containing data at the given time.
!
! Required BARRA filenames are:
!
! sfc_pres-yyyymmddThhmmZ
! mslp-yyyymmddThhmmZ
! sfc_temp-yyyymmddThhmmZ
! temp_scrn-yyyymmddThhmmZ
! qsair_scrn-yyyymmddThhmmZ
! uwnd10m-yyyymmddThhmmZ
! vwnd10m-yyyymmddThhmmZ
! soil_temp-yyyymmddThhmmZ
! soil_mois-yyyymmddThhmmZ
!
! These names are hard coded. They are related to, but are NOT, the standard names of files in the BARRA dataset. 
! The user will have to set up soft links in datadir using these names to link to the actual BARRA files 
!
! Compile with something like:
!
! module purge
! module load intel-compiler/2020.0.166
! module load netcdf/4.7.1
! INCLUDEDIRS="-I/apps/netcdf/4.7.1/include/" 
! LIBDIRS="-L/apps/netcdf/4.7.1/lib/" 
! ifort -o barra_sfc_to_intermediate.exe  $INCLUDEDIRS $LIBDIRS -lnetcdff  -convert big_endian barra_sfc_to_intermediate.f90
!
! with the big_endian flag required on little_endian systems.
!
! Author: Chris Thomas 04/03/2019
! Please send questions, suggestions, bug reports etc to christopherthomas@cmt.id.au
!

module constants

  character(len=*), parameter  :: PREFIX = "BARRA_SFC"                             ! prefix of the intermediate-format output file
  integer,parameter            :: OUTUNIT=10
  
  character(len=*),parameter   :: XDIMNAME='longitude'                             ! dimension and coordinate variable names
  character(len=*),parameter   :: YDIMNAME='latitude'                              ! in the BARRA data file
  character(len=*),parameter   :: XVARN='longitude'
  character(len=*),parameter   :: YVARN='latitude'
  
  integer,parameter            :: IFV = 5                                          ! see WRF Users guide Ch 3 for an explanation
  integer,parameter            :: IPROJ = 0                                        ! of this block of constants
  real,parameter               :: EARTH_RADIUS = 6371.229
  logical,parameter            :: IS_WIND_GRID_REL = .false.
  character(len=32), parameter :: MAP_SOURCE = "BoM BARRA_R reanalysis          "
  real, parameter              :: XFCST = 0.0
  character(len=8),parameter   :: STARTLOC = "SWCORNER"
  real, parameter              :: SFC_LVL = 200100.
  real, parameter              :: SEA_LVL = 201300.

  real,parameter               :: BARRA_FILL_VALUE_LIMIT = -1.E5                   ! data more negative than this will be treated
                                                                                   ! as missing values

end module constants

program barra_to_intemediate

  use netcdf
  use constants

  implicit none
  
  interface

     subroutine parse_input(input,datadir,datetime)
       character(len=288),intent(in)  :: input
       character(len=256),intent(out) :: datadir
       character(len=14),intent(out)  :: datetime
     end subroutine parse_input

     subroutine check(status)
       integer, intent(in) :: status
     end subroutine check
     
     subroutine get_xy_grid(fn,varn,nx,ny,startx,starty,dx,dy)
       character(len=*), intent(in) :: fn
       character(len=*), intent(in) :: varn
       real,intent(out) :: startx,starty,dx,dy
       integer,intent(out) :: nx,ny
     end subroutine get_xy_grid
     
     subroutine getdata1d(fn,varn,dimname,data1d)
       character(len=*), intent(in) :: fn
       character(len=*), intent(in) :: varn
       character(len=*), intent(in) :: dimname
       real,dimension(:),allocatable,intent(inout) :: data1d
     end subroutine getdata1d

     subroutine getdata2d(fn, varn, dimname1, dimname2, data2d) 
       character(len=*), intent(in)                  :: fn,varn,dimname1,dimname2
       real,dimension(:,:),allocatable,intent(inout) :: data2d
     end subroutine getdata2d

     subroutine getdata3d(fn,varn,dimname1,dimname2,dimname3,data3d) 
       character(len=*), intent(in) :: fn,varn,dimname1,dimname2,dimname3
       real,dimension(:,:,:),allocatable,intent(inout) :: data3d
     end subroutine getdata3d
     
     subroutine write_header(hdate, field, units, description, lvl, &
          nx, ny, startx, starty, dx, dy)
       character(len=24), intent(in)           :: hdate
       character(len=9),  intent(in)           :: field
       character(len=25), intent(in)           :: units 
       character(len=46), intent(in)           :: description 
       real,              intent(in)           :: lvl
       integer,           intent(in)           :: nx, ny
       real,              intent(in)           :: startx, starty, dx, dy
     end subroutine write_header

     subroutine write_slab(data,missing_value)
       real,dimension(:,:) :: data
       real,optional       :: missing_value
     end subroutine write_slab

     subroutine interp_soil_data(indata,depth_bnds,outlvl_top,outlvl_bot,outdata)
       real, dimension(:,:,:), intent(in) :: indata
       real, dimension(:,:), intent(in)   :: depth_bnds
       real, intent(in)                   :: outlvl_top, outlvl_bot
       real, dimension(:,:), allocatable, intent(inout)  :: outdata
     end subroutine interp_soil_data
     
     subroutine adjust_soil_moist(data3d,depth_bnds)
       real, dimension(:,:,:), intent(inout)   :: data3d
       real, dimension(:,:), intent(in)        :: depth_bnds
     end subroutine adjust_soil_moist

  end interface


  character (len=288) :: input
  character (len=256) :: datadir
  character (len=14)  :: datetime
  character (len=4)   :: yr
  character (len=2)   :: mth,day,hr

  integer :: nx, ny
  real    :: startx, starty, dx, dy
  real    :: lvl, wps_soil_lvl_top, wps_soil_lvl_bot
  real, dimension(:), allocatable :: lvl_hgt, lvl_hgt_theta, sigma, sigma_theta, hgt, hgt_theta
  real, dimension(:,:), allocatable     :: data2d, depth_bnds, sfc_hgt
  real, dimension(:,:,:), allocatable :: data3d, data_3d

  integer :: i,j,k

  character(len=256) :: barra_fn
  character(len=256) :: out_fn
  character(len=16)  :: barra_varn
  character(len=48)  :: fn_prefix
  character(len=8)   :: fn_suffix
  character(9)       :: field
  character(46)      :: description
  character(25)      :: units
  character(24)      :: hdate
 
  read '(a)', input
  call parse_input(input,datadir,datetime)

  write(*,*) trim(datadir), datetime

  yr = datetime(1:4)
  mth = datetime(5:6)
  day = datetime(7:8)
  hr = datetime(10:11)

  hdate = yr//':'//mth//':'//day//'_'//hr//':00:00     '

 !!!! write SFC data
  
  out_fn = PREFIX//':'//yr//'-'//mth//'-'//day//'_'//hr
  open(unit=OUTUNIT, file=out_fn, status='replace', form='unformatted')

  hdate = yr//'-'//mth//'-'//day//'_'//hr//':00:00     '

  ! get and write the data
  
  barra_varn = 'sfc_pres'
  barra_fn = trim(datadir)//'/'//'sfc_pres-'//datetime
  field = 'PSFC'
  units       = "Pa"
  description = "Surface pressure"
  lvl = SFC_LVL
  call get_xy_grid(barra_fn,barra_varn,nx,ny,startx,starty,dx,dy)
  call getdata2d(barra_fn,barra_varn,YDIMNAME,XDIMNAME,data2d)
  call write_header(hdate, field, units, description, lvl, &
       nx, ny, startx, starty, dx, dy)
  call write_slab(data2d)
  deallocate(data2d)
  
  barra_varn  = 'mslp'
  barra_fn = trim(datadir)//'/'//'mslp-'//datetime
  field    = 'PMSL'
  units       = "Pa"
  description = "Sea-level pressure"
  lvl         = SEA_LVL
  call get_xy_grid(barra_fn,barra_varn,nx,ny,startx,starty,dx,dy)
  call getdata2d(barra_fn,barra_varn,YDIMNAME,XDIMNAME,data2d)
  call write_header(hdate, field, units, description, lvl, &
       nx, ny, startx, starty, dx, dy)
  call write_slab(data2d)
  deallocate(data2d)

  barra_varn  = 'sfc_temp'
  barra_fn = trim(datadir)//'/'//'sfc_temp-'//datetime
  field    = 'SKINTEMP'
  units       = "K"
  description = "Skin temperature"
  lvl         = SFC_LVL
  call get_xy_grid(barra_fn,barra_varn,nx,ny,startx,starty,dx,dy)
  call getdata2d(barra_fn,barra_varn,YDIMNAME,XDIMNAME,data2d)
  call write_header(hdate, field, units, description, lvl, &
       nx, ny, startx, starty, dx, dy)
  call write_slab(data2d)
  deallocate(data2d)

  barra_varn  = 'sfc_temp'
  barra_fn = trim(datadir)//'/'//'sfc_temp-'//datetime
  field    = 'SST'
  units       = "K"
  description = "Sea surface temperature"
  lvl         = SFC_LVL
  call get_xy_grid(barra_fn,barra_varn,nx,ny,startx,starty,dx,dy)
  call getdata2d(barra_fn,barra_varn,YDIMNAME,XDIMNAME,data2d)
  call write_header(hdate, field, units, description, lvl, &
       nx, ny, startx, starty, dx, dy)
  call write_slab(data2d)
  deallocate(data2d)
  
  barra_varn  = 'temp_scrn'
  barra_fn = trim(datadir)//'/'//'temp_scrn-'//datetime
  field    = 'TT'
  units       = "K"
  description = "Temperature at 1.5 metres"
  lvl         = SFC_LVL
  call get_xy_grid(barra_fn,barra_varn,nx,ny,startx,starty,dx,dy)
  call getdata2d(barra_fn,barra_varn,YDIMNAME,XDIMNAME,data2d)
  call write_header(hdate, field, units, description, lvl, &
       nx, ny, startx, starty, dx, dy)
  call write_slab(data2d)
  deallocate(data2d)

  barra_varn  = 'qsair_scrn'
  barra_fn = trim(datadir)//'/'//'qsair_scrn-'//datetime
  field    = 'SPECHUMD'
  units       = "kg kg-1"
  description = "Specific humidity at 1.5 metres"
  lvl         = SFC_LVL
  call get_xy_grid(barra_fn,barra_varn,nx,ny,startx,starty,dx,dy)
  call getdata2d(barra_fn,barra_varn,YDIMNAME,XDIMNAME,data2d)
  call write_header(hdate, field, units, description, lvl, &
          nx, ny, startx, starty, dx, dy)
  call write_slab(data2d,-1.E30)
  deallocate(data2d)

  barra_varn  = 'uwnd10m'
  barra_fn = trim(datadir)//'/'//'uwnd10m-'//datetime
  field    = 'UU'
  units       = "m s-1"
  description = "10 metre wind u component"
  lvl         = SFC_LVL
  call get_xy_grid(barra_fn,barra_varn,nx,ny,startx,starty,dx,dy)
  call getdata2d(barra_fn,barra_varn,YDIMNAME,XDIMNAME,data2d)
  call write_header(hdate, field, units, description, lvl, &
          nx, ny, startx, starty, dx, dy)
  call write_slab(data2d)
  deallocate(data2d)

  barra_varn  = 'vwnd10m'
  barra_fn = trim(datadir)//'/'//'vwnd10m-'//datetime
  field    = 'VV'
  units       = "m s-1"
  description = "10 metre wind v component"
  lvl         = SFC_LVL
  call get_xy_grid(barra_fn,barra_varn,nx,ny,startx,starty,dx,dy)
  call getdata2d(barra_fn,barra_varn,YDIMNAME,XDIMNAME,data2d)
  call write_header(hdate, field, units, description, lvl, &
          nx, ny, startx, starty, dx, dy)
  call write_slab(data2d)
  deallocate(data2d)

  barra_varn       = 'soil_temp'
  barra_fn = trim(datadir)//'/'//'soil_temp-'//datetime
  lvl              = SFC_LVL
  units            = 'K'
  call get_xy_grid(barra_fn,barra_varn,nx,ny,startx,starty,dx,dy)
  call getdata3d(barra_fn,barra_varn,'depth',YDIMNAME,XDIMNAME,data3d)
  
  field         = 'ST000010'
  description      = "T 0-10 cm below ground layer"
  call write_header(hdate, field, units, description, lvl, &
       nx, ny, startx, starty, dx, dy)
  call write_slab(data3d(:,:,1),-1.E30)

  field         = 'ST010035'
  description      = "T 10-35 cm below ground layer"
  call write_header(hdate, field, units, description, lvl, &
       nx, ny, startx, starty, dx, dy)
  call write_slab(data3d(:,:,2),-1.E30)

  field         = 'ST035100'
  description      = "T 35-100 cm below ground layer"
  call write_header(hdate, field, units, description, lvl, &
       nx, ny, startx, starty, dx, dy)
  call write_slab(data3d(:,:,3),-1.E30)

  field         = 'ST100300'
  description      = "T 100-300 cm below ground layer"
  call write_header(hdate, field, units, description, lvl, &
       nx, ny, startx, starty, dx, dy)
  call write_slab(data3d(:,:,4),-1.E30)

  deallocate(data3d)

  barra_varn = 'soil_mois'
  barra_fn = trim(datadir)//'/'//'soil_mois-'//datetime
  lvl        = SFC_LVL
  units      = 'Fraction'
  call get_xy_grid(barra_fn,barra_varn,nx,ny,startx,starty,dx,dy)
  call getdata3d(barra_fn,barra_varn,'depth',YDIMNAME,XDIMNAME,data3d)
  call getdata2d(barra_fn,'depth_bnds','depth','bnds',depth_bnds)
  call adjust_soil_moist(data3d,depth_bnds)

  field   = 'SM000010'
  description = "Soil moisture 0-10 cm below ground layer"
  call write_header(hdate, field, units, description, lvl, &
       nx, ny, startx, starty, dx, dy)
  call write_slab(data3d(:,:,1),-1.E30)

  field   = 'SM010035'
  description = "Soil moisture 10-35 cm below ground layer"
  call write_header(hdate, field, units, description, lvl, &
       nx, ny, startx, starty, dx, dy)
  call write_slab(data3d(:,:,2),-1.E30)

  field   = 'SM035100'
  description = "Soil moisture 35-100 cm below ground layer"
  call write_header(hdate, field, units, description, lvl, &
       nx, ny, startx, starty, dx, dy)
  call write_slab(data3d(:,:,3),-1.E30)

  field   = 'SM100300'
  description = "Soil moisture 100-300 cm below ground layer"
  call write_header(hdate, field, units, description, lvl, &
       nx, ny, startx, starty, dx, dy)
  call write_slab(data3d(:,:,4),-1.E30)

  deallocate(data3d)
  deallocate(depth_bnds)

  close(OUTUNIT)

!!!!! end write SFC data
  
end program barra_to_intemediate

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine parse_input(input,datadir,datetime)

  implicit none

  character(len=288),intent(in)  :: input
  character(len=256),intent(out) :: datadir
  character(len=14),intent(out)  :: datetime

  integer :: n

  n = index(input,' ')
  datadir = input(1:n-1)
  datetime = trim(adjustl(input(n:288)))

  
end subroutine parse_input

subroutine check(status)

  use netcdf
  use constants
  
  implicit none

  integer, intent(in) :: status

  if(status /= nf90_noerr) then 
     print *, trim(nf90_strerror(status))
     stop "Stopped"
  end if

end subroutine check

subroutine get_xy_grid(fn,varn,nx,ny,startx,starty,dx,dy)

  use netcdf
  use constants
  
  implicit none

  character(len=*), intent(in) :: fn
  character(len=*), intent(in) :: varn
  real,intent(out) :: startx,starty,dx,dy
  integer,intent(out) :: nx,ny

  integer :: ncid,dimid,varid
  real,dimension(:),allocatable :: data

  call check(nf90_open(trim(fn),nf90_nowrite,ncid))
  call check(nf90_inq_dimid(ncid,XDIMNAME,dimid))
  call check(nf90_inquire_dimension(ncid,dimid,len=nx))
  allocate(data(nx))
  call check(nf90_inq_varid(ncid,XVARN,varid))
  call check(nf90_get_var(ncid, varid, data))
  startx = data(1)
  dx = data(2) - data(1)
  deallocate(data)
  
  call check(nf90_inq_dimid(ncid,YDIMNAME,dimid))
  call check(nf90_inquire_dimension(ncid,dimid,len=ny))
  allocate(data(ny))
  call check(nf90_inq_varid(ncid,YVARN,varid))
  call check(nf90_get_var(ncid, varid, data))
  starty = data(1)
  dy = data(2) - data(1)
  deallocate(data)

  call check(nf90_close(ncid))

end subroutine get_xy_grid

subroutine getdata1d(fn,varn,dimname,data1d)

  use netcdf
  use constants
  
  implicit none

  character(len=*), intent(in) :: fn
  character(len=*), intent(in) :: varn
  character(len=*), intent(in) :: dimname
  real,dimension(:),allocatable,intent(inout) :: data1d

  integer :: ncid,dimid,varid,n

  call check(nf90_open(trim(fn),nf90_nowrite,ncid))
  
  ! get the dimension and allocate array
  call check(nf90_inq_dimid(ncid,dimname,dimid))
  call check(nf90_inquire_dimension(ncid,dimid,len=n))
  allocate(data1d(n))

  ! get the data
  call check(nf90_inq_varid(ncid,varn,varid))
  call check(nf90_get_var(ncid, varid, data1d))
  call check(nf90_close(ncid))

end subroutine getdata1d

subroutine getdata2d(fn, varn, dimname1, dimname2, data2d) 

  use netcdf
  use constants

  implicit none

  character(len=*), intent(in) :: fn,varn,dimname1,dimname2
  real,dimension(:,:),allocatable,intent(inout) :: data2d

  integer :: ncid,dimid,varid,n1,n2

  call check(nf90_open(trim(fn),nf90_nowrite,ncid))

  ! get the dimensions and allocate array
  call check(nf90_inq_dimid(ncid,dimname1,dimid))
  call check(nf90_inquire_dimension(ncid,dimid,len=n1))
  call check(nf90_inq_dimid(ncid,dimname2,dimid))
  call check(nf90_inquire_dimension(ncid,dimid,len=n2))
  allocate(data2d(n2,n1))

  ! get the data
  call check(nf90_inq_varid(ncid,varn,varid))
  call check(nf90_get_var(ncid, varid, data2d))
  call check(nf90_close(ncid))

end subroutine getdata2d

subroutine getdata3d(fn,varn,dimname1,dimname2,dimname3,data3d) 

  use netcdf
  use constants

  implicit none

  character(len=*), intent(in) :: fn,varn,dimname1,dimname2,dimname3
  real,dimension(:,:,:),allocatable,intent(inout) :: data3d

  integer :: ncid,dimid,varid,n1,n2,n3

  call check(nf90_open(trim(fn),nf90_nowrite,ncid))

  ! get the dimensions and allocate array
  call check(nf90_inq_dimid(ncid,dimname1,dimid))
  call check(nf90_inquire_dimension(ncid,dimid,len=n1))
  call check(nf90_inq_dimid(ncid,dimname2,dimid))
  call check(nf90_inquire_dimension(ncid,dimid,len=n2))
  call check(nf90_inq_dimid(ncid,dimname3,dimid))
  call check(nf90_inquire_dimension(ncid,dimid,len=n3))
  allocate(data3d(n3,n2,n1))

  ! get the data
  call check(nf90_inq_varid(ncid,varn,varid))
  call check(nf90_get_var(ncid, varid, data3d))
  call check(nf90_close(ncid))

end subroutine getdata3d

subroutine write_header(hdate, field, units, description, xlvl, &
     nx, ny, startx, starty, dx, dy)

  use constants

  character(len=24), intent(in)           :: hdate
  character(len=9),  intent(in)           :: field
  character(len=25), intent(in)           :: units 
  character(len=46), intent(in)           :: description 
  real,              intent(in)           :: xlvl
  integer,           intent(in)           :: nx, ny
  real,              intent(in)           :: startx, starty, dx, dy

  write(*,*) 'Writing data for ', field, ' at level ', xlvl

  write(outunit) IFV
  write(outunit) hdate, XFCST, MAP_SOURCE, field, units, description, xlvl , nx, ny, IPROJ
  write(outunit) STARTLOC, starty, startx, dy, dx, EARTH_RADIUS
  write(outunit) IS_WIND_GRD_REL

end subroutine write_header

subroutine write_slab(data,missing_value)

  use constants
  
  implicit none

  real,dimension(:,:) :: data
  real, optional    :: missing_value          

  integer :: i,j

  if (present(missing_value)) then
     do i = 1, size(data,1)
        do j = 1, size(data,2)
           if (data(i,j) <  BARRA_FILL_VALUE_LIMIT) then
              data(i,j) = missing_value
           end if
        end do
     end do
  end if

  write(OUTUNIT) data

end subroutine write_slab
  

subroutine interp_soil_data(indata,depth_bnds,outlvl_top,outlvl_bot,outdata)

  ! this subroutine does volumetric interpolation. That is, we assume that 
  ! the input values are constant over each depth band, and we integrate 
  ! these values over the column of the output layer.  
  !
  ! If a different interpolation method is desired then this subroutine could be 
  ! replaced by another with the same interface

  real, dimension(:,:,:), intent(in) :: indata
  real, dimension(:,:), intent(in)   :: depth_bnds
  real, intent(in)                   :: outlvl_top, outlvl_bot
  real, dimension(:,:),allocatable,intent(inout)  :: outdata

  real, dimension(:), allocatable    :: wt,inlvl_top,inlvl_bot
  real                               :: outlvl_t,outlvl_b
  integer                            :: i, j, il, nlvls, nx, ny

  nx = size(indata,1)
  ny = size(indata,2)
  nlvls = size(indata,3)

  !write(*,*),nx,ny,nlvls

  allocate(inlvl_top(nlvls))
  allocate(inlvl_bot(nlvls))
  allocate(wt(nlvls))
  allocate(outdata(nx,ny))

  ! these are depths so change sign
  inlvl_top  = -1.*depth_bnds(1,:)
  inlvl_bot  = -1.*depth_bnds(2,:)
  outlvl_t = -outlvl_top
  outlvl_b = -outlvl_bot
  ! for each input layer compute the overlaps with the output layer
  ! we use these as the weights in a weighted sum, to do the integration
  tot_wt = 0.
  do il = 1, nlvls
     wt(il) = max(0.,min(inlvl_top(il),outlvl_t) - max(inlvl_bot(il),outlvl_b))
     tot_wt = tot_wt + wt(il)
  end do

  ! do a weighted sum of the input data using these overlaps
  do j = 1, ny
     do i=1, nx
        outdata(i,j) = 0.
        do il = 1, nlvls
           outdata(i,j) = outdata(i,j) + wt(il)*indata(i,j,il)
        end do
        outdata(i,j) = outdata(i,j)/tot_wt
     end do
  end do

end subroutine interp_soil_data

subroutine adjust_soil_moist(data3d,depth_bnds)

  ! soil moisture in the BARRA dataset is given as kg m-2 
  ! and we require it as a fraction in WRF. Density of water
  ! is 1000 kg m-3 and to convert, divide the data by depth 
  ! and density

  real, dimension(:,:,:), intent(inout)   :: data3d
  real, dimension(:,:), intent(in)        :: depth_bnds

  integer                               :: i, j, k, nlvls, nx, ny
  real                                  :: depth
  real, parameter                       :: RHO = 1000.

  nx    = size(data3d,1)
  ny    = size(data3d,2)
  nlvls = size(data3d,3)

  do k = 1, nlvls
     depth = depth_bnds(2,k)-depth_bnds(1,k)
     do j = 1, ny
        do i = 1, nx
           data3d(i,j,k) = data3d(i,j,k) / (depth * RHO)
        end do
     end do
  end do

end subroutine adjust_soil_moist

subroutine interp_lin(xi,vi,xo,vo)
  ! depends on xi being monotonically increasing
  
  implicit none
  
  real, dimension(:), intent(in)    :: xi, vi, xo
  real, dimension(:), intent(inout) :: vo

  integer                           :: ki, ko, ni, ki_lo, ki_hi

  ni = size(xi)
  
  do ko = 1, size(xo)
     ! find the bracketing indices
     do ki = 2, ni
        ki_hi = ki
        if (xi(ki) > xo(ko)) then
           exit
        end if
     end do
     ki_lo = ki_hi - 1

     vo(ko) = vi(ki_lo) + (xo(ko)-xi(ki_lo))*(vi(ki_hi) - vi(ki_lo))/(xi(ki_hi) - xi(ki_lo))
     
  end do
  
end subroutine interp_lin




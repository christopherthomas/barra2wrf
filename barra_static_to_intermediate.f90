! Reads the BoM BARRA static data and outputs files in the WPS intermediate format
!
! NOTE: this program only has to be run once, the data are static (time-independent)
!
! Useage: echo datadir datetime | barra_static_to_intermediate.exe
!
! where
!
! datadir  is a directory containing the BARRA files or a directory of links to the BARRA files, 
! datetime is the datetime stamp in the form yyyymmddThhmmZ for example 20170409T0000Z
!          (note the T and Z are obligatory). Unlike for the surface and model (or pressure) level data,
!          there is nothing special about the date time string. It does not form part of the input
!          file name, but the output file name will be constructed from it. For example 0000000T0000Z is
!          a perfectly good value, in which case the output file will be called BARRA_INV:0000-00-00_00, and this
!          must be the value of the constants_name field in the metgrid namelist.
!
! Required BARRA file names are:
!
! topog
! lnd_mask
!
! These names are hard coded. They are related to, but are NOT, the standard names of files in the BARRA dataset. 
! The user will have to set up soft links in datadir using these names to link to the actual BARRA files 
!
! Compile with something like:
!
! module purge
! module load intel-compiler/2020.0.166
! module load netcdf/4.7.1
! INCLUDEDIRS="-I/apps/netcdf/4.7.1/include/" 
! LIBDIRS="-L/apps/netcdf/4.7.1/lib/" 
! ifort -o barra_static_to_intermediate.exe  $INCLUDEDIRS $LIBDIRS -lnetcdff  -convert big_endian barra_static_to_intermediate.f90
!
! with the big_endian flag required on little_endian systems.
!
! Author: Chris Thomas 04/03/2019
! Please send questions, suggestions, bug reports etc to christopherthomas@cmt.id.au
!

module constants

  character(len=*), parameter  :: PREFIX = "BARRA_INV"                         ! prefix of the intermediate-format output file
  integer,parameter            :: OUTUNIT=10
  
  character(len=*),parameter   :: XDIMNAME='longitude'                         ! dimension and coordinate variable names
  character(len=*),parameter   :: YDIMNAME='latitude'                          ! in the BARRA data file
  character(len=*),parameter   :: XVARN='longitude'
  character(len=*),parameter   :: YVARN='latitude'
  
  integer,parameter            :: IFV = 5                                      ! see WRF Users guide Ch 3 for an explanation
  integer,parameter            :: IPROJ = 0                                    ! of this block of constants
  real,parameter               :: EARTH_RADIUS = 6371.229
  logical,parameter            :: IS_WIND_GRID_REL = .false.
  character(len=32), parameter :: MAP_SOURCE = "BoM BARRA_R reanalysis          "
  real, parameter              :: XFCST = 0.0
  character(len=8),parameter   :: STARTLOC = "SWCORNER"
  real, parameter              :: SFC_LVL = 200100.
  real, parameter              :: SEA_LVL = 201300.

  real,parameter               :: BARRA_FILL_VALUE_LIMIT = -1.E5               ! data more negative than this will be treated
                                                                               ! as missing values

end module constants

program barra_static_to_intemediate

  use netcdf
  use constants

  implicit none
  
  interface

     subroutine parse_input(input,datadir,datetime)
       character(len=288),intent(in)  :: input
       character(len=256),intent(out) :: datadir
       character(len=14),intent(out)  :: datetime
     end subroutine parse_input

     subroutine check(status)
       integer, intent(in) :: status
     end subroutine check

     subroutine get_xy_grid(fn,varn,nx,ny,startx,starty,dx,dy)
       character(len=*), intent(in) :: fn
       character(len=*), intent(in) :: varn
       real,intent(out) :: startx,starty,dx,dy
       integer,intent(out) :: nx,ny
     end subroutine get_xy_grid
     
     subroutine getdata2d(fn, varn, dimname1, dimname2, data2d) 
       character(len=*), intent(in)                  :: fn,varn,dimname1,dimname2
       real,dimension(:,:),allocatable,intent(inout) :: data2d
     end subroutine getdata2d

     subroutine write_header(hdate, field, units, description, lvl, &
          nx, ny, startx, starty, dx, dy)
       character(len=24), intent(in)           :: hdate
       character(len=9),  intent(in)           :: field
       character(len=25), intent(in)           :: units 
       character(len=46), intent(in)           :: description 
       real,              intent(in)           :: lvl
       integer,           intent(in)           :: nx, ny
       real,              intent(in)           :: startx, starty, dx, dy
     end subroutine write_header

     subroutine write_slab(data,missing_value)
       real,dimension(:,:) :: data
       real,optional       :: missing_value
     end subroutine write_slab

  end interface


  character (len=288) :: input
  character (len=256) :: datadir
  character (len=14)  :: datetime
  character (len=4)   :: yr
  character (len=2)   :: mth,day,hr

  integer :: nx,ny
  real    :: startx,starty,dx,dy
  real    :: lvl,wps_soil_lvl_top,wps_soil_lvl_bot
  real, dimension(:), allocatable :: lvls
  real, dimension(:,:), allocatable     :: data2d, depth_bnds
  real, dimension(:,:,:), allocatable :: data3d

  integer :: k

  character(len=256) :: barra_fn
  character(len=256) :: out_fn
  character(len=16) :: barra_varn
  character(len=4)   :: cat
  character(9)      :: field
  character(46)     :: description
  character(25)     :: units
  character(24)     :: hdate
 
  read '(a)', input
  call parse_input(input,datadir,datetime)

  yr = datetime(1:4)
  mth = datetime(5:6)
  day = datetime(7:8)
  hr = datetime(10:11)

  hdate = yr//':'//mth//':'//day//'_'//hr//':00:00     '

  
  out_fn = PREFIX//':'//yr//'-'//mth//'-'//day//'_'//hr
  open(unit=OUTUNIT, file=out_fn, status='replace', form='unformatted')

  hdate = yr//'-'//mth//'-'//day//'_'//hr//':00:00     '

  ! get and write the data
  
  barra_varn  = 'topog'
  cat='slv'
  field    = 'SOILHGT'
  units       = "m"
  description = "Height of topography above the geoid"
  lvl         = SFC_LVL
  barra_fn = trim(datadir)//'/'//'topog'
  call get_xy_grid(barra_fn,barra_varn,nx,ny,startx,starty,dx,dy)
  call getdata2d(barra_fn,barra_varn,YDIMNAME,XDIMNAME,data2d)
  call write_header(hdate, field, units, description, lvl, &
       nx, ny, startx, starty, dx, dy)
  call write_slab(data2d)
  deallocate(data2d)
 
  barra_varn  = 'lnd_mask'
  cat='slv'
  field    = 'LANDSEA'
  units       = "fraction"
  description = "Land-sea mask (0=water, 1=land)"
  lvl         = SFC_LVL
  barra_fn = trim(datadir)//'/'//'lnd_mask'
  call get_xy_grid(barra_fn,barra_varn,nx,ny,startx,starty,dx,dy)
  call getdata2d(barra_fn,barra_varn,YDIMNAME,XDIMNAME,data2d)
  call write_header(hdate, field, units, description, lvl, &
          nx, ny, startx, starty, dx, dy)
  call write_slab(data2d)
  deallocate(data2d)
  
  close(OUTUNIT)

end program barra_static_to_intemediate

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine parse_input(input,datadir,datetime)

  implicit none

  character(len=288),intent(in)  :: input
  character(len=256),intent(out) :: datadir
  character(len=14),intent(out)  :: datetime

  integer :: n

  n = index(input,' ')
  datadir = input(1:n-1)
  datetime = trim(adjustl(input(n:288)))

  
end subroutine parse_input

subroutine check(status)

  use netcdf
  use constants
  
  implicit none

  integer, intent(in) :: status

  if(status /= nf90_noerr) then 
     print *, trim(nf90_strerror(status))
     stop "Stopped"
  end if

end subroutine check

subroutine get_xy_grid(fn,varn,nx,ny,startx,starty,dx,dy)

  use netcdf
  use constants
  
  implicit none

  character(len=*), intent(in) :: fn
  character(len=*), intent(in) :: varn
  real,intent(out) :: startx,starty,dx,dy
  integer,intent(out) :: nx,ny

  integer :: ncid,dimid,varid
  real,dimension(:),allocatable :: data

  call check(nf90_open(trim(fn),nf90_nowrite,ncid))
  call check(nf90_inq_dimid(ncid,XDIMNAME,dimid))
  call check(nf90_inquire_dimension(ncid,dimid,len=nx))
  allocate(data(nx))
  call check(nf90_inq_varid(ncid,XVARN,varid))
  call check(nf90_get_var(ncid, varid, data))
  startx = data(1)
  dx = data(2) - data(1)
  deallocate(data)
  
  call check(nf90_inq_dimid(ncid,YDIMNAME,dimid))
  call check(nf90_inquire_dimension(ncid,dimid,len=ny))
  allocate(data(ny))
  call check(nf90_inq_varid(ncid,YVARN,varid))
  call check(nf90_get_var(ncid, varid, data))
  starty = data(1)
  dy = data(2) - data(1)
  deallocate(data)

  call check(nf90_close(ncid))

end subroutine get_xy_grid

subroutine getdata2d(fn, varn, dimname1, dimname2, data2d) 

  use netcdf
  use constants

  implicit none

  character(len=*), intent(in) :: fn,varn,dimname1,dimname2
  real,dimension(:,:),allocatable,intent(inout) :: data2d

  integer :: ncid,dimid,varid,n1,n2

  call check(nf90_open(trim(fn),nf90_nowrite,ncid))

  ! get the dimensions and allocate array
  call check(nf90_inq_dimid(ncid,dimname1,dimid))
  call check(nf90_inquire_dimension(ncid,dimid,len=n1))
  call check(nf90_inq_dimid(ncid,dimname2,dimid))
  call check(nf90_inquire_dimension(ncid,dimid,len=n2))
  allocate(data2d(n2,n1))

  ! get the data
  call check(nf90_inq_varid(ncid,varn,varid))
  call check(nf90_get_var(ncid, varid, data2d))
  call check(nf90_close(ncid))

end subroutine getdata2d

subroutine write_header(hdate, field, units, description, xlvl, &
     nx, ny, startx, starty, dx, dy)

  use constants

  character(len=24), intent(in)           :: hdate
  character(len=9),  intent(in)           :: field
  character(len=25), intent(in)           :: units 
  character(len=46), intent(in)           :: description 
  real,              intent(in)           :: xlvl
  integer,           intent(in)           :: nx, ny
  real,              intent(in)           :: startx, starty, dx, dy

  write(*,*) 'Writing data for ', field, ' at level ', xlvl

  write(outunit) IFV
  write(outunit) hdate, XFCST, MAP_SOURCE, field, units, description, xlvl , nx, ny, IPROJ
  write(outunit) STARTLOC, starty, startx, dy, dx, EARTH_RADIUS
  write(outunit) IS_WIND_GRD_REL

end subroutine write_header

subroutine write_slab(data,missing_value)

  use constants
  
  implicit none

  real,dimension(:,:) :: data
  real, optional    :: missing_value          

  integer :: i,j

  if (present(missing_value)) then
     do i = 1, size(data,1)
        do j = 1, size(data,2)
           if (data(i,j) <  BARRA_FILL_VALUE_LIMIT) then
              data(i,j) = missing_value
           end if
        end do
     end do
  end if

  write(OUTUNIT) data

end subroutine write_slab
  



